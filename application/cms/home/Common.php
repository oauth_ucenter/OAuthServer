<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace app\cms\home;

use app\index\controller\Home;
use think\Db;
use util\Tree;

/**
 * 前台公共控制器
 * @package app\cms\admin
 */
class Common extends Home
{
    /**
     * 初始化方法
     *
     */
    protected function _initialize()
    {
        parent::_initialize();

        // 获取菜单
        $this->getNav();
        // 获取滚动图片
        $this->assign('slider', $this->getSlider());
        // 获取客服
        $this->assign('support', $this->getSupport());
    }
    
    /**
     * 获取导航
     */
    private function getNav()
    {
        $list_nav = \app\cms\model\Column::getNavData();
        
        foreach ($list_nav as $key => $data) {
            $this->assign($key, Tree::toLayer($data));
        }
    }

    /**
     * 获取滚动图片
     *
     */
    private function getSlider()
    {
        return Db::name('cms_slider')->where('status', 1)->select();
    }

    /**
     * 获取在线客服
     *
     */
    private function getSupport()
    {
        return Db::name('cms_support')->where('status', 1)->order('sort')->select();
    }
}