<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

/**
 * 模块信息
 */
return [
    'name' => 'myoa',
    'title' => '项目管理',
    'identifier' => 'myoa.lhp.module',
    'icon' => 'fa fa-fw fa-users',
    'description' => '简单项目管理<br> 作者邮箱：dragon_lhp@163.com',
    'author' => 'Paul.li',
    'author_url' => 'http://www.cybwnt.com/',
    'version' => '1.0.0',
    'need_module' => [],
    'need_plugin' => [],
    'tables' => [
        'myoa_client',
        'myoa_contract',
        'myoa_object',
        'myoa_status',
        'myoa_stype',
    ],
    'database_prefix' => 'dp_',
    'config' => [

    ],
    'action' => [],
    'access' => [],
];
