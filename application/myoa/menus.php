<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

/**
 * 菜单信息
 */
return [
    [
        'title' => '项目管理',
        'icon' => 'fa fa-fw fa-th-list',
        'url_type' => 'module_admin',
        'url_value' => 'myoa/object/index',
        'url_target' => '_self',
        'online_hide' => 0,
        'sort' => 100,
        'child' => [
            [
                'title' => '客户管理',
                'icon' => 'fa fa-fw fa-list',
                'url_type' => 'module_admin',
                'url_value' => 'myoa/client/index',
                'url_target' => '_self',
                'online_hide' => 0,
                'sort' => 100,
                'child' => [
                    [
                        'title' => '添加',
                        'icon' => 'fa fa-fw fa-list',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/client/add',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ], [
                        'title' => '编辑',
                        'icon' => 'fa fa-fw fa-edit',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/client/edit',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ]
                ]
            ], [
                'title' => '项目管理',
                'icon' => 'fa fa-fw fa-list',
                'url_type' => 'module_admin',
                'url_value' => 'myoa/object/index',
                'url_target' => '_self',
                'online_hide' => 0,
                'sort' => 100,
                'child' => [
                    [
                        'title' => '添加',
                        'icon' => 'fa fa-fw fa-list',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/object/add',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ], [
                        'title' => '编辑',
                        'icon' => 'fa fa-fw fa-edit',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/object/edit',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ]
                ]
            ], [
                'title' => '合同管理',
                'icon' => 'fa fa-fw fa-list',
                'url_type' => 'module_admin',
                'url_value' => 'myoa/contract/index',
                'url_target' => '_self',
                'online_hide' => 0,
                'sort' => 100,
                'child' => [
                    [
                        'title' => '添加',
                        'icon' => 'fa fa-fw fa-list',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/contract/add',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ], [
                        'title' => '编辑',
                        'icon' => 'fa fa-fw fa-edit',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/contract/edit',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ]
                ],
            ], [
                'title' => '状态管理',
                'icon' => 'fa fa-fw fa-list',
                'url_type' => 'module_admin',
                'url_value' => 'myoa/status/index',
                'url_target' => '_self',
                'online_hide' => 0,
                'sort' => 100,
                'child' => [
                    [
                        'title' => '添加',
                        'icon' => 'fa fa-fw fa-list',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/status/add',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ], [
                        'title' => '编辑',
                        'icon' => 'fa fa-fw fa-edit',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/status/edit',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ]
                ],

            ], [
                'title' => '类型管理',
                'icon' => 'fa fa-fw fa-list',
                'url_type' => 'module_admin',
                'url_value' => 'myoa/type/index',
                'url_target' => '_self',
                'online_hide' => 0,
                'sort' => 100,
                'child' => [
                    [
                        'title' => '添加',
                        'icon' => 'fa fa-fw fa-list',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/type/add',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ], [
                        'title' => '编辑',
                        'icon' => 'fa fa-fw fa-edit',
                        'url_type' => 'module_admin',
                        'url_value' => 'myoa/type/edit',
                        'url_target' => '_self',
                        'online_hide' => 0,
                        'sort' => 100,
                    ]
                ],

            ],
        ],
    ],
];
