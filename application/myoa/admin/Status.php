<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/22
 * 功能说明:
 */

namespace app\myoa\admin;


use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\myoa\model\StatusModel;

class Status extends Admin
{
    public function index()
    {
        cookie('__forward__', $_SERVER['REQUEST_URI']);
        // 查询
        $map = $this->getMap();
        // 数据列表
        $data_list = StatusModel::where($map)->column(true);

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setSearch(['type' => '类别', 'name' => '名称'])// 设置搜索框
            ->addFilter('type', 'name')// 添加筛选
            ->addColumns([ // 批量添加数据列
                ['id', 'ID'],
                ['type', '类别', 'select', StatusModel::STATUS],
                ['name', '名称', 'text.edit'],
                ['sort', '排序', 'text.edit'],
                ['status', '状态', 'switch'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete')// 批量添加顶部按钮
            ->addRightButtons(['edit', 'delete'])// 批量添加右侧按钮
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function add($pid = 0)
    {
        // 保存数据
        if ($this->request->isPost()) {
            // 表单数据
            $data = $this->request->post();

//            // 验证
//            $result = $this->validate($data, 'Column');
//            if (true !== $result) $this->error($result);

            if ($column = StatusModel::create($data)) {
                cache('myoa_Status', null);
                // 记录行为
                action_log('Status_add', 'myoa_Status', $column['id'], UID, $data['name']);
                $this->success('新增成功', 'index');
            } else {
                $this->error('新增失败');
            }
        }

        // 显示添加页面
        return ZBuilder::make('form')
            ->addFormItems([
                ['select', 'type', '类别', '<span class="text-danger">必填</span>', StatusModel::STATUS, 1],
                ['text', 'name', '名称', '<span class="text-danger">必填</span>'],
                ['text', 'sort', '排序', '', 100],
                ['radio', 'status', '立即启用', '', ['否', '是'], 1],
            ])
            ->fetch();
    }

    public function edit($id = '')
    {
        if ($id === 0) $this->error('参数错误');

        // 保存数据
        if ($this->request->isPost()) {
            $data = $this->request->post();

            if (StatusModel::update($data)) {
                // 记录行为
                action_log('Status_edit', 'myoa_Status', $id, UID, $data['name']);
                return $this->success('编辑成功', 'index');
            } else {
                return $this->error('编辑失败');
            }
        }

        // 获取数据
        $info = StatusModel::get($id);
        // 显示编辑页面
        return ZBuilder::make('form')
            ->addFormItems([
                ['hidden', 'id'],
                ['select', 'type', '类别', '<span class="text-danger">必填</span>', StatusModel::STATUS, 1],
                ['text', 'name', '名称', '<span class="text-danger">必填</span>'],
                ['text', 'sort', '排序', '', 100],
                ['radio', 'status', '立即启用', '', ['否', '是'], 1],
            ])
            ->setFormData($info)
            ->fetch();
    }
}