<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/22
 * 功能说明:
 */

namespace app\myoa\admin;

use app\admin\controller\Admin;
use think\Request;

class Common extends Admin
{
    static $app = null;

    public function __construct(Request $request = null)
    {
        parent::__construct($request);
    }


}