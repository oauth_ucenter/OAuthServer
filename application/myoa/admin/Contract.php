<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/23
 * 功能说明:
 */

namespace app\myoa\admin;


use app\admin\controller\Admin;
use app\admin\model\Attachment;
use app\common\builder\ZBuilder;
use app\myoa\model\ClientModel;
use app\myoa\model\ContractModel;
use app\myoa\model\ObjectModel;
use app\myoa\model\StatusModel;

class contract extends Admin
{

    public function index()
    {

        cookie('__forward__', $_SERVER['REQUEST_URI']);
        // 查询
        $map = $this->getMap();
        // 排序
        $order = $this->getOrder('update_time desc');
        // 数据列表
        $data_list = ContractModel::where($map)->order($order)->column(true);


        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setSearch(['name' => '客户名称', 'ink_name' => '联系人'])// 设置搜索框
            ->addColumns([ // 批量添加数据列
                ['id', 'ID'],
                ['number', '合同编号'],
                ['client_id', '客户', ClientModel::getIDAndName()],
                ['object_id', '项目名称', ObjectModel::getIDAndName()],
                ['start_date', '合同开始日期'],
                ['end_date', '合同结束日期'],
                ['attachment_id', '合同附件', 'callback', function ($id) {
                    $Attachment = new Attachment();
                    $path = $Attachment->getFilePath($id);
                    $name = $Attachment->getFileName($id);
                    return "<a href='$path'>$name</a>";
                }],
                ['remark', '备注'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButtons('add,delete')// 批量添加顶部按钮
            ->addRightButtons(['edit', 'delete'])// 批量添加右侧按钮
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function add($pid = 0)
    {
        // 保存数据
        if ($this->request->isPost()) {
            // 表单数据
            $data = $this->request->post();

            if ($column = ContractModel::create($data)) {
                cache('myoa_contract', null);
                // 记录行为
                action_log('contract_add', 'myoa_contract', $column['id'], UID, $data['number']);
                $this->success('新增成功', 'index');
            } else {
                $this->error('新增失败');
            }
        }

        // 显示添加页面
        return ZBuilder::make('form')
            ->addFormItems([
                ['text', 'number', '合同编号', '<span class="text-danger">必填</span>'],
                ['select', 'client_id', '客户名称', '<span class="text-danger">必填</span>', ClientModel::getIDAndName()],
                ['select', 'object_id', '项目名称', '<span class="text-danger">必选</span>', ObjectModel::getIDAndName()],
                ['date', 'start_date', '合同开始日期'],
                ['date', 'end_date', '合同结束日期'],
                ['file', 'attachment_id', '附件'],
                ['textarea', 'remark', '备注'],
            ])
            ->fetch();
    }

    public function edit($id = '')
    {
        if ($id === 0) $this->error('参数错误');

        // 保存数据
        if ($this->request->isPost()) {
            $data = $this->request->post();

            if (ContractModel::update($data)) {
                // 记录行为
                action_log('contract_edit', 'myoa_contract', $id, UID, $data['number']);
                return $this->success('编辑成功', 'index');
            } else {
                return $this->error('编辑失败');
            }
        }

        // 获取数据
        $info = ContractModel::get($id);
        // 显示编辑页面
        return ZBuilder::make('form')
            ->addFormItems([
                ['hidden', 'id'],
                ['static', 'number', '合同编号', '<span class="text-danger">必填</span>','',true],
                ['select', 'client_id', '客户名称', '<span class="text-danger">必填</span>', ClientModel::getIDAndName()],
                ['select', 'object_id', '项目名称', '<span class="text-danger">必选</span>', ObjectModel::getIDAndName()],
                ['date', 'start_date', '合同开始日期'],
                ['date', 'end_date', '合同结束日期'],
                ['file', 'attachment_id', '附件'],
                ['textarea', 'remark', '备注'],
            ])
            ->setFormData($info)
            ->fetch();
    }
}