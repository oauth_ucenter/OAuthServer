<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/22
 * 功能说明:
 */

namespace app\myoa\admin;


use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\myoa\model\ClientModel;
use app\myoa\model\StatusModel;

class Client extends Admin {
    
    public function index(){
        
        cookie('__forward__', $_SERVER[ 'REQUEST_URI' ]);
        // 查询
        $map = $this->getMap();
        // 排序
        $order = $this->getOrder('update_time desc');
        // 数据列表
        $data_list = ClientModel::where($map)->order($order)->column(true);
        
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')->setSearch(['name' => '客户名称', 'ink_name' => '联系人'])// 设置搜索框
            ->addColumns([ // 批量添加数据列
                           ['id', 'ID'],
                           ['name', '客户名称'],
                           ['link_name', '联系人'],
                           ['link_position', '职位'],
                           ['link_tel', '联系电话'],
                           ['link_qq', 'QQ'],
                           ['link_wechat', '微信'],
                           ['status', '状态', StatusModel::getStart(['type' => 1])],
                           ['remark', '备注'],
                           ['right_button', '操作', 'btn']
            ])->addTopButtons('add,delete')// 批量添加顶部按钮
            ->addRightButtons(['edit', 'delete'])// 批量添加右侧按钮
            ->setRowList($data_list)// 设置表格数据
            ->fetch(); // 渲染模板
    }
    
    public function add($pid = 0){
        
        // 保存数据
        if($this->request->isPost()) {
            // 表单数据
            $data = $this->request->post();
            
            //            // 验证
            //            $result = $this->validate($data, 'Column');
            //            if (true !== $result) $this->error($result);
            
            if($column = ClientModel::create($data)) {
                cache('myoa_client', null);
                // 记录行为
                action_log('client_add', 'myoa_client', $column[ 'id' ], UID, $data[ 'name' ]);
                $this->success('新增成功', 'index');
            }else {
                $this->error('新增失败');
            }
        }
        
        // 显示添加页面
        return ZBuilder::make('form')->addFormItems([
                ['text', 'name', '客户名称', '<span class="text-danger">必填</span>'],
                ['text', 'link_name', '联系人', '<span class="text-danger">必填</span>'],
                ['text', 'link_position', '职位', '<span class="text-danger">必填</span>'],
                ['text', 'link_tel', '联系电话', '<span class="text-danger">必选</span>'],
                ['text', 'link_qq', 'QQ号码'],
                ['text', 'link_wechat', '微信'],
                ['select', 'status', '类别', '', StatusModel::getStart(['type' => 1]), 1],
                ['text', 'sort', '排序', '', 100],
                ['text', 'remark', '备注'],
            ])->fetch();
    }
    
    public function edit($id = ''){
        
        if($id === 0)
            $this->error('参数错误');
        
        // 保存数据
        if($this->request->isPost()) {
            $data = $this->request->post();
            
            if(ClientModel::update($data)) {
                // 记录行为
                action_log('client_edit', 'myoa_client', $id, UID, $data[ 'name' ]);
                return $this->success('编辑成功', 'index');
            }else {
                return $this->error('编辑失败');
            }
        }
        
        // 获取数据
        $info = ClientModel::get($id);
        // 显示编辑页面
        return ZBuilder::make('form')->addFormItems([
                ['hidden', 'id'],
                ['text', 'name', '客户名称', '<span class="text-danger">必填</span>'],
                ['text', 'link_name', '联系人', '<span class="text-danger">必填</span>'],
                ['text', 'link_position', '职位', '<span class="text-danger">必填</span>'],
                ['text', 'link_tel', '联系电话', '<span class="text-danger">必选</span>'],
                ['text', 'link_qq', 'QQ号码'],
                ['text', 'link_wechat', '微信'],
                ['select', 'status', '类别', '', StatusModel::getStart(['type' => 1]), 1],
                ['text', 'sort', '排序', '', 100],
                ['text', 'remark', '备注'],
            ])->setFormData($info)->fetch();
    }
}