<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/23
 * 功能说明:
 */

namespace app\myoa\model;

class StatusModel extends CommonModel
{
    // 设置当前模型对应的完整数据表名称
    protected $table = '__MYOA_STATUS__';

    const STATUS = [
        1 => '客户类型',
        2 => '项目状态',
    ];


    static public function getStart($map)
    {
        return static::where($map)->column('id,name');
    }
}