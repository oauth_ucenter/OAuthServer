--
-- 表的结构 `dp_myoa_client`
--
 DROP TABLE IF EXISTS `dp_myoa_client`;
CREATE TABLE `dp_myoa_client` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `ink_name` varchar(255) DEFAULT NULL,
  `link_tel` varchar(255) DEFAULT NULL COMMENT '客户电话',
  `link_qq` varchar(255) DEFAULT NULL,
  `link_wechat` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `dp_myoa_contract`
--
   DROP TABLE IF EXISTS `dp_myoa_contract`;
CREATE TABLE `dp_myoa_contract` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `number` varchar(255) DEFAULT NULL COMMENT '合同编号',
  `client_id` int(11) DEFAULT NULL COMMENT '客户ID',
  `object_id` int(11) DEFAULT NULL COMMENT '项目ID',
  `start_date` datetime DEFAULT NULL COMMENT '合同开始日期',
  `end_date` datetime DEFAULT NULL COMMENT '合同结束日期',
  `attachment_id` int(11) DEFAULT NULL COMMENT '附件',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `dp_myoa_object`
--
DROP TABLE IF EXISTS `dp_myoa_object`;
CREATE TABLE `dp_myoa_object` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '项目名称',
  `leader` varchar(255) DEFAULT NULL COMMENT '负责人',
  `client_id` int(11) DEFAULT NULL,
  `object_type` int(11) DEFAULT NULL COMMENT '项目类型',
  `creat_object_date` datetime DEFAULT NULL COMMENT '项目签单时间',
  `contract_id` int(11) DEFAULT NULL COMMENT '合同编号',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `dp_myoa_status`
--
DROP TABLE IF EXISTS `dp_myoa_status`;
CREATE TABLE `dp_myoa_status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(250) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `dp_myoa_type`
--
DROP TABLE IF EXISTS `dp_myoa_type`;
CREATE TABLE `dp_myoa_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(250) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;


INSERT INTO `dp_myoa_status` VALUES ('1', '1', '普通客户', '100', '1'), ('2', '1', 'VIP客户', '100', '1'), ('3', '1', '老客户', '100', '1'), ('4', '1', '合作伙伴', '100', '1'), ('5', '1', '潜在客户', '100', '1'), ('6', '2', '洽谈期', '100', null), ('7', '2', '开发期', '100', null), ('8', '2', '测试期', '100', null), ('9', '2', '交付期', '100', null), ('10', '2', '维护期', '100', null), ('11', '2', '合同到期', '100', null), ('12', '2', '拟定合同', '100', null);

INSERT INTO `dp_myoa_type` VALUES ('1', '1', '定制官网', '100', null), ('2', '1', 'web系统', '100', null), ('3', '1', '其他', '100', null);

