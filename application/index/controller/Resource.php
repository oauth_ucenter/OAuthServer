<?php

	namespace app\index\controller;


	class Resource extends Home
	{

		/**
		 * This is called by the client app once the client has obtained an access
		 * token for the current user.  If the token is valid, the resource (in this
		 * case, the "friends" of the current user) will be returned to the client
		 */
		public function index()
		{
			if (!$this->oauth_server->verifyResourceRequest($this->oauth_request, $this->oauth_response)) {
				return $this->oauth_server->getResponse();
			} else {
				// return a fake API response - not that exciting
				// @TODO return something more valuable, like the name of the logged in user
				$api_response = array(
					'friends' => array(
						'john',
						'matt',
						'jane'
					)
				);
				return json_encode($api_response);
			}
		}
	}
