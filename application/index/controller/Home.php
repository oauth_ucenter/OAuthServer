<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

	namespace app\index\controller;

	use OAuth2\Storage\Memory;
	use app\common\controller\Common;

	use app\wechat\model\WeChat;
	use EasyWeChat\Foundation\Application;
	use OAuth2\HttpFoundationBridge\Request;
	use plugins\Qrcode\controller\Qrcode;

	use OAuth2\HttpFoundationBridge\Response as BridgeResponse;
	use OAuth2\Server as OAuth2Server;
	use OAuth2\Storage\Pdo;
	use OAuth2\OpenID\GrantType\AuthorizationCode;
	use OAuth2\GrantType\UserCredentials;
	use OAuth2\GrantType\RefreshToken;

	/**
	 * 前台公共控制器
	 * @package app\index\controller
	 */
	class Home extends Common
	{
		protected $oauth_server   = null;
		protected $oauth_response = null;
		protected $oauth_request  = null;

		/**
		 * 初始化方法
		 *
		 */
		protected function _initialize()
		{
			// 系统开关
			if (!config('web_site_status')) {
				$this->error('站点已经关闭，请稍后访问~');
			}


			// ensure our Sqlite database exists
			if (file_exists($sqliteFile = ROOT_PATH . '/data/oauth.sqlite')) {
//            $this->generateSqliteDb();
			}

			// create PDO-based sqlite storage
			$storage = new Pdo(array('dsn' => 'sqlite:' . $sqliteFile));

			// create array of supported grant types
			$grantTypes = array(
				'authorization_code' => new AuthorizationCode($storage),
				'user_credentials'   => new UserCredentials($storage),
				'refresh_token'      => new RefreshToken($storage, array(
					'always_issue_new_refresh_token' => true,
				)),
			);

			// instantiate the oauth server
			$server = new OAuth2Server($storage, array(
				'enforce_state'      => true,
				'allow_implicit'     => true,
				'use_openid_connect' => true,
				'issuer'             => $_SERVER['HTTP_HOST'],
			), $grantTypes);

			$server->addStorage($this->getKeyStorage(), 'public_key');

			// add the server to the silex "container" so we can use it in our controllers (see src/OAuth2Demo/Server/Controllers/.*)
			$this->oauth_server = $server;

			/**
			 * add HttpFoundataionBridge Response to the container, which returns a silex-compatible response object
			 * @see (https://github.com/bshaffer/oauth2-server-httpfoundation-bridge)
			 */
			$this->oauth_response = new BridgeResponse();

			$this->oauth_request = Request::createFromGlobals();
		}


		public function getKeyStorage()
		{
			$publicKey = file_get_contents(ROOT_PATH . '/data/pubkey.pem');
			$privateKey = file_get_contents(ROOT_PATH . '/data/privkey.pem');

			// create storage
			$keyStorage = new Memory(array(
				'keys' => array(
					'public_key'  => $publicKey,
					'private_key' => $privateKey,
				)
			));

			return $keyStorage;
		}

		/**
		 * combineURL
		 * 拼接url
		 * @param string $baseURL 基于的url
		 * @param array $keysArr 参数列表数组
		 * @return string           返回拼接的url
		 */
		static public function combineURL($baseURL, $keysArr)
		{
			$combined = $baseURL . "?";
			$valueArr = array();

			foreach ($keysArr as $key => $val) {
				$valueArr[] = "$key=$val";
			}

			$keyStr = implode("&", $valueArr);
			$combined .= ($keyStr);

			return $combined;
		}
	}
