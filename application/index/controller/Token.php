<?php

namespace app\index\controller;

class Token extends Home
{
    function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
        config('default_return_type', 'json');
    }

    /**
     * This is called by the client app once the client has obtained
     * an authorization code from the Authorize Controller (@see OAuth2Demo\Server\Controllers\Authorize).
     * If the request is valid, an access token will be returned
     */
    public function token()
    {

        // get the oauth server (configured in src/OAuth2Demo/Server/Server.php)
        $server = $this->oauth_server;

        // get the oauth response (configured in src/OAuth2Demo/Server/Server.php)
        $response = $this->oauth_response;

        // let the oauth2-server-php library do all the work!
        $server->handleTokenRequest($this->oauth_request, $response);
        return $response->getContent();
    }
}
