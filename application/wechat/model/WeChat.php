<?php
/**
 * 开发工具: PhpStorm.
 * 作   者: mybook-lhp
 * 日   期: 17/11/19
 * 功能说明:
 */

namespace app\wechat\model;

use EasyWeChat\Foundation\Application;
use think\Model;

class WeChat extends Model
{
    static $app = null;


    static public function Main($configs=[])
    {
       $config=  config()['wechat_config']; 
        if(!empty($config)){
           foreach ($configs as $key=> $value) {
            $config[$key] = $value;
           } 
        }
      
       return self::$app = new Application($config);
    }

}