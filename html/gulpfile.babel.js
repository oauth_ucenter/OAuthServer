import gulp from 'gulp';
import plumber from 'gulp-plumber'; // 捕获错误
import sourcemaps from 'gulp-sourcemaps'; // 资源文件
import sass from 'gulp-sass'; //编译sass
import del from 'del';
import autoprefixer from 'gulp-autoprefixer'; //自动补全浏览器前缀
const source = ['sass/**/*.scss'];
const dest = 'dist/css/';

gulp.task('sass', () => {
    return gulp.src(source)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: true,
            remove: true
        }))
        .pipe(gulp.dest(dest));
});

gulp.task('clean', del.bind(null, ['dist']));

gulp.task('build', ['sass', 'watch']);

gulp.task('watch', () => {
    gulp.watch(source, ['sass']);
});

gulp.task('default', ['clean'], () => {
    gulp.start('build');
});