<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace plugins\Block\controller;


use app\common\controller\Common;
use util\File;

/**
 * 插件后台管理控制器
 * @package plugins\HelloWorld\controller
 */
class View extends Common
{
    static public $ConfigJson = [];
    static public $StaticInfo = [];
    static public $BlockInfo = [];


    static public function getAllConfig()
    {

        $ConfigPath = File::getDir(dirname(dirname(__FILE__)) . '/view/');
        $ConfigInfo = [];
        foreach ($ConfigPath as $cPath) {
            if (strstr($cPath, 'config.json')) {
                $Info = json_decode(file_get_contents($cPath));
                $ConfigInfo[$Info->info->name] = $Info;
                self::$ConfigJson[$Info->info->identifier] = $Info;
            }

            if (strstr($cPath, '.js')) {
                self::$StaticInfo['js'][] = '/' . str_replace(ROOT_PATH, '', $cPath);
            } elseif (strstr($cPath, '.css')) {
                self::$StaticInfo['css'][] = '/' . str_replace(ROOT_PATH, '', $cPath);
            }
        }
    }

    /**
     * 插件管理页
     * @return mixed
     */
    static public function staticInfo($type = null)
    {

        if ($type !== null) {
            return self::$StaticInfo[$type];
        }
        return self::$StaticInfo;
    }

    /**
     * 插件管理页
     * @return mixed
     */
    static public function Html()
    {
        return ['block_paul_block' => 'block'];
    }


    static function searchDir($path, &$data)
    {
        if (is_dir($path)) {
            $dp = dir($path);
            while ($file = $dp->read()) {
                if ($file != '.' && $file != '..') {
                    self::searchDir($path . '/' . $file, $data);
                }
            }
            $dp->close();
        }
        if (is_file($path)) {
            $data[] = $path;
        }
    }

    static function getDir($dir)
    {
        $data = array();
        static::searchDir($dir, $data);
        return $data;
    }

}
