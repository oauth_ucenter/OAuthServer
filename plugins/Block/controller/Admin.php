<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace plugins\Block\controller;

use app\cms\model\Document;
use app\common\builder\ZBuilder;
use app\common\controller\Common;
use plugins\Block\model\BlockModel;
use plugins\Block\model\Block;


/**
 * 插件后台管理控制器
 * @package plugins\Block\controller
 */
class Admin extends Common
{
    /**
     * 插件管理页
     * @return mixed
     */
    public function index()
    {
        // 查询条件
        $map = $this->getMap();

        BlockModel::getAllConfig();
        $ConfigJson = [];
//        dump(BlockModel::$ConfigJson);
        $i = 0;
        foreach (BlockModel::$ConfigJson as $key => $item) {
            $ConfigJson[$i]['id'] = $i;
            $ConfigJson[$i]['name'] = $item->info->name;
            $ConfigJson[$i]['title'] = $item->info->title;
            $ConfigJson[$i]['version'] = $item->info->version;
            $ConfigJson[$i]['description'] = $item->info->description;
            $i++;
        };

        // 自定义按钮
        $btnTwo = [
            'title' => '自定义按钮2',
            'icon' => 'fa fa-user',
            'href' => plugin_url('Block/Admin/blockForm', ['name' => 'molly', 'age' => 12]),
        ];

        $btnBack = [
            'title' => '返回插件列表',
            'icon' => 'fa fa-reply',
            'href' => url('plugin/index'),
            'class' => 'btn btn-warning',
        ];

        // 用TableBuilder渲染模板
        return ZBuilder::make('table')
            ->setPageTitle('模块列表')
            ->addColumn('id', 'ID')
            ->addColumns([
                ['name', '名称'],
                ['title', '标题'],
                ['version', '版本'],
                ['description', '说明'],
                ['status', '状态', 'switch', ['未安装', '启用', '禁用'], 0],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButton('custom', $btnBack)
            ->addTopButtons('enable,disable,delete')
            ->addRightButton('edit', ['plugin_name' => 'Block'])
            ->addRightButtons('enable,disable,delete')
            ->setTableName('plugin_hello')
            ->setRowList($ConfigJson)
//            ->setPages($page)
            ->fetch();
    }

    /**
     * 新增
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            // 验证数据
            $result = $this->validate($data, [
                'name|出处' => 'require',
                'said|名言' => 'require',
            ]);
            if (true !== $result) {
                // 验证失败 输出错误信息
                return $this->error($result);
            }

            // 插入数据
            if (Block::create($data)) {
                return $this->success('新增成功', cookie('__forward__'));
            } else {
                return $this->error('新增失败');
            }
        }

        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('新增')
            ->addFormItem('text', 'name', '出处')
            ->addFormItem('text', 'said', '名言')
            ->fetch();
    }

    /**
     * 编辑
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();

            // 使用自定义的验证器验证数据
            $validate = new BlockValidate();
            if (!$validate->check($data)) {
                // 验证失败 输出错误信息
                return $this->error($validate->getError());
            }

            // 更新数据
            if (Block::update($data)) {
                return $this->success('编辑成功', cookie('__forward__'));
            } else {
                return $this->error('编辑失败');
            }
        }

        $id = input('param.id');

        // 获取数据
        $info = Block::get($id);

        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('编辑')
            ->addFormItem('hidden', 'id')
            ->addFormItem('text', 'name', '出处')
            ->addFormItem('text', 'said', '名言')
            ->setFormData($info)
            ->fetch();
    }

    /**
     * 插件自定义方法
     * @return mixed
     */
    public function blockTable()
    {
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('table')
            ->setPageTitle('插件自定义方法(列表)')
            ->setSearch(['said' => '名言', 'name' => '出处'])
            ->addColumn('id', 'ID')
            ->addColumn('said', '名言')
            ->addColumn('name', '出处')
            ->addColumn('status', '状态', 'switch')
            ->addColumn('right_button', '操作', 'btn')
            ->setTableName('plugin_hello')
            ->fetch();
    }

    /**
     * 插件自定义方法
     * 这里的参数是根据插件定义的按钮链接按顺序设置
     * @param string $id
     * @param string $table
     * @param string $name
     * @param string $age
     * @return mixed
     */
    public function blockForm($id = '', $table = '', $name = '', $age = '')
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            halt($data);
        }

        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('插件自定义方法(表单)')
            ->addFormItem('text', 'name', '出处')
            ->addFormItem('text', 'said', '名言')
            ->fetch();
    }

    /**
     * 自定义页面
     * @return mixed
     */
    public function blockPage()
    {
        // 1.使用默认的方法渲染模板，必须指定完整的模板文件名（包括模板后缀）
//        return $this->fetch(config('plugin_path'). 'Block/view/index.html');

        // 2.使用已封装好的快捷方法，该方法只用于加载插件模板
        // 如果不指定模板名称，则自动加载插件view目录下与当前方法名一致的模板
        return $this->pluginView();
//         return $this->pluginView('index'); // 指定模板名称
//         return $this->pluginView('', 'tpl'); // 指定模板后缀
    }
}
