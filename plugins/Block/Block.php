<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace plugins\Block;

use app\common\controller\Plugin;
use plugins\Block\model\BlockModel;

/**
 * 系统环境信息插件
 * @package plugins\DevTeam
 */
class Block extends Plugin
{
    /**
     * @var array 插件信息
     */
    public $info = [
        // 插件名[必填]
        'name' => 'Block',
        // 插件标题[必填]
        'title' => '测试插件',
        // 插件唯一标识[必填],格式：插件名.开发者标识.plugin
        'identifier' => 'Block.paul.plugin',
        // 插件图标[选填]
        'icon' => 'fa fa-fw fa-users',
        // 插件描述[选填]
        'description' => '测试插件',
        // 插件作者[必填]
        'author' => 'Paul',
        // 作者主页[选填]
        'author_url' => 'http://www.cybwnt.com',
        // 插件版本[必填],格式采用三段式：主版本号.次版本号.修订版本号
        'version' => '1.0.0',
        // 是否有后台管理功能[选填]
        'admin' => '1',
    ];

    /**
     * @var array 插件钩子
     */
    public $hooks = [
        'page_plugin_css',
        'page_plugin_html',
        'page_plugin_js',
    ];


    /**
     * 模块css钩子
     */
    public function pagePluginCss($type)
    {
        $Css = BlockModel::staticInfo('css');

        foreach ($Css as $key => $css) {
            echo "<link rel='stylesheet' href='{$css}'>";
        }

    }

    /**
     * 模块内容钩子
     */
    public function pagePluginHtml($type)
    {
        $Data = BlockModel::$ConfigJson;

        foreach ($Data as $key => $datum) {

            $this->assign('IMG', str_replace(ROOT_PATH, '', dirname(dirname(__FILE__)) . '/Block/view/') . $key);

            $this->fetch($key . '/' . $datum->info->name, (array)$datum->default);
        }
    }

    /**
     * 模块js钩子
     */
    public function pagePluginJs($type)
    {
        $JS = BlockModel::staticInfo('js');
        foreach ($JS as $key => $js) {
            echo "<script type='application/javascript'  src='{$js}'></script>";
        }
    }


    /**
     * 安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 卸载方法必
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }

}